﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Timers;

namespace Lab2_Fitz
{
    public partial class MainWindow : Window
    {
        #region peremennie
        Random x = new Random();
        Random y = new Random();
        public Point p = new Point();
        public Point[] pi = new Point[2];
        int Status_exp = 0;
        DateTime Start;
        DateTime Stopped;
        TimeSpan Elapsed = new TimeSpan();
        public Point s; 
        #endregion

        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonStart_Click(object sender, RoutedEventArgs e)
        {
            lbRez.Items.Clear();
            lbRez.Items.Add("Результати");
            Expir();
        }

        private Point DrowObject(Point p)
        {
            Rectangle el = new Rectangle();
            el.Width = 7;
            el.Height = 14;
            p.X = x.Next(Convert.ToInt32(this.Width - el.Width));
            p.Y = y.Next(Convert.ToInt32(icDrow.ActualHeight - el.Height));
            el.Fill = Brushes.Black;
            InkCanvas.SetLeft(el, p.X);
            InkCanvas.SetTop(el, p.Y);
            icDrow.Children.Add(el);
            return p;
        }

        public void Expir()
        {
            int[] rez_arr = new int[2];
            Array.Clear(pi, 0, pi.Length - 1);
            Start = new DateTime(0);
            icDrow.Children.Clear();
            icDrow.Strokes.Clear();
            for (int i=0; i<2; i++)
            {
                pi[i] = DrowObject(p);
            }
            Status_exp++;
        }

        private void IcUp(object sender, MouseButtonEventArgs e)
        {
            if (Status_exp > 0)
            {
                Point[] n = Array.FindAll(pi, element => (Math.Abs(element.X - e.GetPosition(this).X) < 14) && (Math.Abs(element.Y - e.GetPosition(this).Y) < 28));

                if (Array.Exists(pi, element => (Math.Abs(element.X - e.GetPosition(this).X) < 7) && (Math.Abs(element.Y - e.GetPosition(this).Y) < 14)))
                {
                    if (!((Math.Abs(s.X - e.GetPosition(this).X) < 7) && (Math.Abs(s.Y - e.GetPosition(this).Y) < 7)))
                    {
                        s = e.GetPosition(this);
                        if (Start.Ticks == 0)
                        {
                            Start = DateTime.Now;
                        }
                        else
                        {
                            Stopped = DateTime.Now;
                            Elapsed = Stopped.Subtract(Start);
                            long elapsedTicks = Stopped.Ticks - Start.Ticks;
                            TimeSpan elapsedSpan = new TimeSpan(elapsedTicks);
                            double rez = Math.Sqrt(Math.Pow(pi[0].X - pi[1].X, 2) + Math.Pow(pi[0].Y - pi[1].Y, 2));
                            lbRez.Items.Add("Експеримент " + Status_exp.ToString() + " - Час: " + elapsedSpan.Milliseconds.ToString() + " мс, Відстань: " + rez + " пкс");
                            if (Status_exp < 100) Expir();
                        }
                    }
                }
            }
        }
    }
}
